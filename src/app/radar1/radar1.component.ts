import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-radar1',
  templateUrl: './radar1.component.html',
  styleUrls: ['./radar1.component.css'],
})
export class Radar1Component implements OnInit {
  margin = { top: 100, right: 100, bottom: 100, left: 100 };
  width =
    Math.min(700, window.innerWidth - 10) -
    this.margin.left -
    this.margin.right;
  height = Math.min(
    this.width,
    window.innerHeight - this.margin.top - this.margin.bottom - 20
  );

  data = [
    [
      //iPhone
      { axis: 'calm', value: 0.22 },
      { axis: 'dark', value: 0.28 },
      { axis: 'energetic', value: 0.29 },
      { axis: 'happy', value: 0.17 },
      { axis: 'melancholic', value: 0.22 },
      { axis: 'relaxed', value: 0.02 },
      { axis: 'tense', value: 0.21 },
      { axis: 'uplifting', value: 0.5 },
    ],
    [
      //Samsung
      { axis: 'calm', value: 0.27 },
      { axis: 'dark', value: 0.16 },
      { axis: 'energetic', value: 0.35 },
      { axis: 'happy', value: 0.13 },
      { axis: 'melancholic', value: 0.2 },
      { axis: 'relaxed', value: 0.13 },
      { axis: 'tense', value: 0.35 },
      { axis: 'uplifting', value: 0.38 },
    ],
  ];

  color = d3.scaleOrdinal().range(['#d3d3d3', '#cc333f']);

  radarChartOptions = {
    w: this.width,
    h: this.height,
    margin: this.margin,
    maxValue: 1,
    levels: 1,
    roundStrokes: true,
    color: this.color,
  };

  constructor() {}
  ngOnInit() {
    this.RadarChart('.radarChart1', this.data, this.radarChartOptions);
  }

  RadarChart(id, data, options) {
    var cfg,
      maxValue,
      allAxis,
      rScale,
      svg,
      g,
      filter,
      axisGrid,
      axis,
      radarLine,
      blobWrapper,
      blobCircleWrapper,
      total,
      radius,
      feMerge,
      feMergeNode_1,
      feMergeNode_2,
      angleSlice,
      tooltip,
      newX,
      newY,
      feGaussianBlur;

    //chart configuration

    cfg = {
      w: 600, //Width of the circle
      h: 600, //Height of the circle
      margin: { top: 20, right: 20, bottom: 20, left: 20 }, //The margins of the SVG
      levels: 3, //How many levels or inner circles should there be drawn
      maxValue: 0, //What is the value that the biggest circle will represent
      labelFactor: 1.3, //How much farther than the radius of the outer circle should the labels be placed
      wrapWidth: 60, //The number of pixels after which a label needs to be given a new line
      opacityArea: 0.35, //The opacity of the area of the blob
      dotRadius: 4, //The size of the colored circles of each blog
      opacityCircles: 0.1, //The opacity of the circles of each blob
      strokeWidth: 2, //The width of the stroke around each blob
      roundStrokes: false, //If true the area and stroke will follow a round path (cardinal-closed)
      color: d3.scaleOrdinal(d3.schemeCategory10), //Color function
    };

    //Put all of the options into a variable called cfg
    if ('undefined' !== typeof options) {
      for (var i in options) {
        if ('undefined' !== typeof options[i]) {
          cfg[i] = options[i];
        }
      }
    }

    maxValue = Math.max(
      cfg.maxValue,
      d3.max(data, function (i) {
        return d3.max(
          i.map(function (o) {
            return o.value;
          })
        );
      })
    );

    (allAxis = data[0].map(function (i, j) {
      return i.axis;
    })), //Names of each axis
      (total = allAxis.length), //The number of different axes
      (radius = Math.min(cfg.w / 2.5, cfg.h / 2.5)), //Radius of the outermost circle
      (angleSlice = (Math.PI * 2) / total); //The width in radians of each "slice"

    //Scale for the radius
    rScale = d3.scaleLinear().range([0, radius]).domain([0, maxValue]);

    /////////////////////////////////////////////////////////
    //////////// Create the container SVG and g /////////////
    /////////////////////////////////////////////////////////

    //Remove whatever chart with the same id/class was present before
    d3.select(id).select('svg').remove();

    //Initiate the radar chart SVG
    svg = d3
      .select(id)
      .append('svg')
      .attr('width', cfg.w + cfg.margin.left + cfg.margin.right)
      .attr('height', cfg.h + cfg.margin.top + cfg.margin.bottom)
      .attr('class', 'radar' + id);

    //Append a g element
    g = svg
      .append('g')
      .attr(
        'transform',
        'translate(' +
          (cfg.w / 2 + cfg.margin.left) +
          ',' +
          (cfg.h / 2 + cfg.margin.top) +
          ')'
      );

    //Filter for the outside glow
    (filter = g.append('defs').append('filter').attr('id', 'glow')),
      (feGaussianBlur = filter
        .append('feGaussianBlur')
        .attr('stdDeviation', '2.5')
        .attr('result', 'coloredBlur')),
      (feMerge = filter.append('feMerge')),
      (feMergeNode_1 = feMerge.append('feMergeNode').attr('in', 'coloredBlur')),
      (feMergeNode_2 = feMerge
        .append('feMergeNode')
        .attr('in', 'SourceGraphic'));

    axisGrid = g.append('g').attr('class', 'axisWrapper');

    //Draw the background circles
    axisGrid
      .selectAll('.levels')
      .data(d3.range(1, cfg.levels + 1).reverse())
      .enter()
      .append('circle')
      .attr('class', 'gridCircle')
      .style('opacity', '1')
      .attr('r', function (d, i) {
        return (radius / cfg.levels) * d;
      })
      .style('fill', '#CDCDCD')
      .style('stroke', '#CDCDCD')
      .style('fill-opacity', cfg.opacityCircles);

    //Create the straight lines radiating outward from the center
    axis = axisGrid
      .selectAll('.axis')
      .data(allAxis)
      .enter()
      .append('g')
      // .style('transform-origin', '0 0')
      // .style('transform', 'scale(0)')
      .attr('class', 'axis');

    //Append the lines
    axis
      .append('line')
      .attr('x1', 0)
      .attr('y1', 0)
      .attr('x2', function (d, i) {
        return rScale(maxValue) * Math.cos(angleSlice * i - Math.PI / 2);
      })
      .attr('y2', function (d, i) {
        return rScale(maxValue) * Math.sin(angleSlice * i - Math.PI / 2);
      })
      .attr('class', 'line')
      .style('stroke', '#CDCDCD')
      .style('stroke-width', '1px');

    //Append the labels at each axis
    axis
      .append('text')
      .attr('class', 'legend')
      .style('font-size', '14px')
      .attr('text-anchor', 'middle')
      .attr('fill', '#333')
      .style('opacity', '1')
      .attr('dy', '0.35em')
      .attr('x', function (d, i) {
        return (
          rScale(maxValue * cfg.labelFactor) *
          Math.cos(angleSlice * i - Math.PI / 2)
        );
      })
      .attr('y', function (d, i) {
        return (
          rScale(maxValue * cfg.labelFactor) *
          Math.sin(angleSlice * i - Math.PI / 2)
        );
      })
      .text(function (d) {
        return d;
      })
      .call(wrap, cfg.wrapWidth);

    /////////////////////////////////////////////////////////
    ///////////// Draw the radar chart blobs ////////////////
    /////////////////////////////////////////////////////////
    //The radial line function
    radarLine = d3
      .lineRadial()
      .radius(function (d, i) {
        console.log(d.value);
        return d.value;
      })
      .angle(function (d, i) {
        console.log(i);

        return i * angleSlice;
      });

    //Create a wrapper for the blobs
    blobWrapper = g
      .selectAll('.radarWrapper')
      .data(data)
      .enter()
      .append('g')
      .attr('d', radarLine(data))
      .attr('class', 'radarWrapper');

    //Append the backgrounds
    blobWrapper
      .append('path')
      .attr('class', 'radarArea')
      .attr('d', function (d, i) {
        return radarLine(d);
      })
      .style('fill', function (d, i) {
        return cfg.color(i);
      })
      .style('fill-opacity', cfg.opacityArea)
      .on('mouseover', function (d, i) {
        //Dim all blobs
        d3.selectAll('.radarArea')
          .transition()
          .duration(200)
          .style('fill-opacity', 0.1);
        //Bring back the hovered over blob
        d3.select(this).transition().duration(200).style('fill-opacity', 0.7);
      })
      .on('mouseout', function () {
        //Bring back all blobs
        d3.selectAll('.radarArea')
          .transition()
          .duration(200)
          .style('fill-opacity', cfg.opacityArea);
      });

    //Create the outlines
    blobWrapper
      .append('path')
      .attr('class', 'radarStroke')
      .attr('d', function (d, i) {
        return radarLine(d);
      })
      .style('stroke-width', cfg.strokeWidth + 'px')
      .style('stroke', function (d, i) {
        return cfg.color(i);
      })
      .style('fill', 'none')
      .style('filter', 'url(#glow)');

    //Append the circles
    blobWrapper
      .selectAll('.radarCircle')
      .data(function (d, i) {
        return d;
      })
      .enter()
      .append('circle')
      .attr('class', 'radarCircle')
      .attr('r', cfg.dotRadius)
      .attr('cx', function (d, i) {
        return rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2);
      })
      .attr('cy', function (d, i) {
        return rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2);
      })
      .style('fill', function (d, i, j) {
        return cfg.color(j);
      })
      .style('fill-opacity', 0.8);

    /////////////////////////////////////////////////////////
    //////// Append invisible circles for tooltip ///////////
    /////////////////////////////////////////////////////////

    //Wrapper for the invisible circles on top
    blobCircleWrapper = g
      .selectAll('.radarCircleWrapper')
      .data(data)
      .enter()
      .append('g')
      .attr('class', 'radarCircleWrapper');

    //Append a set of invisible circles on top for the mouseover pop-up
    blobCircleWrapper
      .selectAll('.radarInvisibleCircle')
      .data(function (d, i) {
        return d;
      })
      .enter()
      .append('circle')
      .attr('class', 'radarInvisibleCircle')
      .attr('r', cfg.dotRadius * 1.5)
      .attr('cx', function (d, i) {
        return rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2);
      })
      .attr('cy', function (d, i) {
        return rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2);
      })
      .style('fill', 'none')
      .style('pointer-events', 'all')
      .on('mouseover', function (d, i) {
        newX = parseFloat(d3.select(this).attr('cx')) - 10;
        newY = parseFloat(d3.select(this).attr('cy')) - 10;

        tooltip
          .attr('x', newX)
          .attr('y', newY)
          .text(d.value)
          .transition()
          .duration(200)
          .style('opacity', 1);
      })
      .on('mouseout', function () {
        tooltip.transition().duration(200).style('opacity', 0);
      });

    //wrap
    function wrap(text, width) {
      text.each(function () {
        var text = d3.select(this),
          words = text.text().split(/\s+/).reverse(),
          word,
          line = [],
          lineNumber = 0,
          lineHeight = 1.4, // ems
          y = text.attr('y'),
          x = text.attr('x'),
          dy = parseFloat(text.attr('dy')),
          tspan = text
            .text(null)
            .append('tspan')
            .attr('x', x)
            .attr('y', y)
            .attr('dy', dy + 'em');

        while ((word = words.pop())) {
          line.push(word);
          tspan.text(line.join(' '));
          if (tspan.node().getComputedTextLength() > width) {
            line.pop();
            tspan.text(line.join(' '));
            line = [word];
            tspan = text
              .append('tspan')
              .attr('x', x)
              .attr('y', y)
              .attr('dy', ++lineNumber * lineHeight + dy + 'em')
              .text(word);
          }
        }
      });
    } //wrap
  }
}
