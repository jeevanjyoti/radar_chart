import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Radar1Component } from './radar1.component';

describe('Radar1Component', () => {
  let component: Radar1Component;
  let fixture: ComponentFixture<Radar1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Radar1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Radar1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
