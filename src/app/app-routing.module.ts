import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RadarComponent } from './radar/radar.component';
import { Radar1Component } from './radar1/radar1.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'radar', component: RadarComponent },
  { path: 'radar1', component: Radar1Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
