import { Component, OnInit, Input } from '@angular/core';
import * as d3 from 'd3';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-radar',
  templateUrl: './radar.component.html',
  styleUrls: ['./radar.component.css'],
})
export class RadarComponent implements OnInit {
  @Input() mood_data;
  @Input() count;
  @Input() width;
  @Input() height;
  private song1 = 'assets/sampleJSON1.jscsrc';

  //radar chart
  margin: any;
  songData: any[] = [];
  finalData = [];
  trackColors = [];
  radarChartOptions = {};
  data = [
    [
      //iPhone
      { axis: 'calm', value: 0.22 },
      { axis: 'dark', value: 0.28 },
      { axis: 'energetic', value: 0.29 },
      { axis: 'happy', value: 0.17 },
      { axis: 'melancholic', value: 0.22 },
      { axis: 'relaxed', value: 0.02 },
      { axis: 'tense', value: 0.21 },
      { axis: 'uplifting', value: 0.5 },
    ],
    [
      //Samsung
      { axis: 'calm', value: 0.27 },
      { axis: 'dark', value: 0.16 },
      { axis: 'energetic', value: 0.35 },
      { axis: 'happy', value: 0.13 },
      { axis: 'melancholic', value: 0.2 },
      { axis: 'relaxed', value: 0.13 },
      { axis: 'tense', value: 0.35 },
      { axis: 'uplifting', value: 0.38 },
    ],
  ];

  constructor(private http: HttpClient) {}

  RadarChart(id, data, options) {
    let newX;
    let newY;
    var cfg = {
      w: 700,
      h: 700,
      margin: { top: 20, right: 20, bottom: 20, left: 20 }, //The margins of the SVG
      levels: 3, //How many levels or inner circles should there be drawn
      maxValue: 0, //What is the value that the biggest circle will represent
      labelFactor: 1.25, //How much farther than the radius of the outer circle should the labels be placed
      wrapWidth: 60, //The number of pixels after which a label needs to be given a new line
      opacityArea: 0.35, //The opacity of the area of the blob
      dotRadius: 4, //The size of the colored circles of each blog
      opacityCircles: 0.1, //The opacity of the circles of each blob
      strokeWidth: 2, //The width of the stroke around each blob
      roundStrokes: false, //If true the area and stroke will follow a round path (cardinal-closed)
      color: d3.scaleOrdinal(d3.schemeCategory10), //Color function
    };

    //Put all of the options into a variable called cfg
    if ('undefined' !== typeof options) {
      for (var i in options) {
        if ('undefined' !== typeof options[i]) {
          cfg[i] = options[i];
        }
      } //for i
    } //if

    //If the supplied maxValue is smaller than the actual one, replace by the max in the data
    var maxValue = Math.max(
      cfg.maxValue,
      d3.max(data, function (i) {
        return d3.max(
          i.map(function (o) {
            return o.value;
          })
        );
      })
    );

    var allAxis = data[0].map(function (i, j) {
        return i.axis;
      }), //Names of each axis
      total = allAxis.length, //The number of different axes
      radius = Math.min(cfg.w / 2, cfg.h / 2), //Radius of the outermost circle
      Format = d3.format('%'), //Percentage formatting
      angleSlice = (Math.PI * 2) / total; //The width in radians of each "slice"

    //Scale for the radius
    var rScale = d3.scaleLinear().range([0, 230]).domain([0, maxValue]);

    /////////////////////////////////////////////////////////
    //////////// Create the container SVG and g /////////////
    /////////////////////////////////////////////////////////

    //Remove whatever chart with the same id/class was present before
    d3.select(id).select('svg').remove();

    //Initiate the radar chart SVG
    var svg = d3
      .select(id)
      .append('svg')
      .attr('width', cfg.w + cfg.margin.left + cfg.margin.right)
      .attr('height', cfg.h + cfg.margin.top + cfg.margin.bottom)
      .attr('class', 'radar' + id);
    //Append a g element
    var g = svg
      .append('g')
      .attr(
        'transform',
        'translate(' +
          (cfg.w / 2 + cfg.margin.left) +
          ',' +
          (cfg.h / 2 + cfg.margin.top) +
          ')'
      );

    /////////////////////////////////////////////////////////
    ////////// Glow filter for some extra pizzazz ///////////
    /////////////////////////////////////////////////////////

    //Filter for the outside glow
    var filter = g.append('defs').append('filter').attr('id', 'glow'),
      feGaussianBlur = filter
        .append('feGaussianBlur')
        .attr('stdDeviation', '2.5')
        .attr('result', 'coloredBlur'),
      feMerge = filter.append('feMerge'),
      feMergeNode_1 = feMerge.append('feMergeNode').attr('in', 'coloredBlur'),
      feMergeNode_2 = feMerge.append('feMergeNode').attr('in', 'SourceGraphic');

    /////////////////////////////////////////////////////////
    /////////////// Draw the Circular grid //////////////////
    /////////////////////////////////////////////////////////

    //Wrapper for the grid & axes
    var axisGrid = g.append('g').attr('class', 'axisWrapper');

    //Draw the background circles

    var grad = svg
      .append('defs')
      .append('linearGradient')
      .attr('id', 'grad')
      .attr('x1', '0%')
      .attr('x2', '0%')
      .attr('y1', '0%')
      .attr('y2', '100%');
    var colors = ['rgba(207,207,210,1)', 'rgba(242,242,242,1)'];

    grad
      .selectAll('stop')
      .data(colors)
      .enter()
      .append('stop')
      .style('stop-color', function (d) {
        return d;
      })
      .attr('offset', function (d, i) {
        return 100 * (i / (colors.length - 1)) + '%';
      });

    axisGrid
      .selectAll('.levels')
      .data(d3.range(1, cfg.levels + 1).reverse())
      .enter()
      .append('circle')
      .attr('class', 'gridCircle')
      .attr('r', function (d, i) {
        return 220;
      })
      .style('fill', 'url(#grad)')
      .style('stroke', '#CDCDCD')
      .style('fill-opacity', '1')
      .style('filter', 'url(#glow)');

    /////////////////////////////////////////////////////////
    //////////////////// Draw the axes //////////////////////
    /////////////////////////////////////////////////////////

    //Create the straight lines radiating outward from the center
    var axis = axisGrid
      .selectAll('.axis')
      .data(allAxis)
      .enter()
      .append('g')
      .attr('class', 'axis');
    //Append the lines
    axis
      .append('line')
      .attr('x1', 0)
      .attr('y1', 0)
      .attr('x2', function (d, i) {
        return rScale(maxValue * 1.1) * Math.cos(angleSlice * i - Math.PI / 2);
      })
      .attr('y2', function (d, i) {
        return rScale(maxValue * 1.1) * Math.sin(angleSlice * i - Math.PI / 2);
      })
      .attr('class', 'line')
      .style('stroke', '#A1A1A4')
      .style('stroke-width', '1px');

    //Append the labels at each axis
    axis
      .append('text')
      .attr('class', 'legend')
      .style('font-size', '14px')
      .attr('text-anchor', 'middle')
      .attr('dy', '0.35em')
      .attr('x', function (d, i) {
        return (
          rScale(maxValue * cfg.labelFactor) *
          Math.cos(angleSlice * i - Math.PI / 2)
        );
      })
      .attr('y', function (d, i) {
        return (
          rScale(maxValue * cfg.labelFactor) *
          Math.sin(angleSlice * i - Math.PI / 2)
        );
      })
      .text(function (d) {
        return d;
      })
      .call(wrap, cfg.wrapWidth);

    //The radial line function
    var radarLine = d3
      .lineRadial()
      .radius(function (d) {
        return rScale(d.value);
      })
      .angle(function (d, i) {
        return i * angleSlice;
      });

    // if (cfg.roundStrokes) {
    //   radarLine.interpolate('cardinal-closed');
    // }

    //Create a wrapper for the blobs
    var blobWrapper = g
      .selectAll('.radarWrapper')
      .data(data)
      .enter()
      .append('g')
      .attr('class', 'radarWrapper');

    //Append the backgrounds
    blobWrapper
      .append('path')
      .attr('class', 'radarArea')
      .attr('d', function (d, i) {
        return radarLine(d);
      })
      .style('fill', function (d, i) {
        console.log(i);

        return cfg.color(i);
      })
      .style('fill-opacity', cfg.opacityArea)
      .on('mouseover', function (d, i) {
        //Dim all blobs
        d3.selectAll('.radarArea')
          .transition()
          .duration(200)
          .style('fill-opacity', 0);
        //Bring back the hovered over blob
        d3.select(this).transition().duration(200).style('fill-opacity', 1);
      })
      .on('mouseout', function () {
        //Bring back all blobs
        d3.selectAll('.radarArea')
          .transition()
          .duration(200)
          .style('fill-opacity', cfg.opacityArea);
      });

    // //Create the outlines
    // blobWrapper
    //   .append('path')
    //   .attr('class', 'radarStroke')
    //   .attr('d', function (d, i) {
    //     return radarLine(d);
    //   })
    //   .style('stroke-width', cfg.strokeWidth + 'px')
    //   .style('stroke', function (d, i) {
    //     return cfg.color(i);
    //   })
    //   .style('fill', 'none')
    //   .style('filter', 'url(#glow)');

    // var blobCircleWrapper = g
    //   .selectAll('.radarCircleWrapper')
    //   .data(data)
    //   .enter()
    //   .append('g')
    //   .attr('class', 'radarCircleWrapper');

    // blobCircleWrapper
    //   .selectAll('.radarInvisibleCircle')
    //   .data(function (d, i) {
    //     return d;
    //   })
    //   .enter()
    //   .append('circle')
    //   .attr('class', 'radarInvisibleCircle')
    //   .attr('r', cfg.dotRadius * 1.5)
    //   .attr('cx', function (d, i) {
    //     return rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2);
    //   })
    //   .attr('cy', function (d, i) {
    //     return rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2);
    //   })
    //   .style('fill', 'none')
    //   .style('pointer-events', 'all')
    //   .on('mouseover', function (d, i) {
    //     newX = parseFloat(d3.select(this).attr('cx')) - 10;
    //     newY = parseFloat(d3.select(this).attr('cy')) - 10;

    //     tooltip
    //       .attr('x', newX)
    //       .attr('y', newY)
    //       .text(Format(d.value))
    //       .transition()
    //       .duration(200)
    //       .style('opacity', 1);
    //   })
    //   .on('mouseout', function () {
    //     tooltip.transition().duration(200).style('opacity', 0);
    //   });

    // var tooltip = g.append('text').attr('class', 'tooltip').style('opacity', 0);

    function wrap(text, width) {
      text.each(function () {
        var text = d3.select(this),
          words = text.text().split(/\s+/).reverse(),
          word,
          line = [],
          lineNumber = 0,
          lineHeight = 1.4, // ems
          y = text.attr('y'),
          x = text.attr('x'),
          dy = parseFloat(text.attr('dy')),
          tspan = text
            .text(null)
            .append('tspan')
            .attr('x', x)
            .attr('y', y)
            .attr('dy', dy + 'em');

        while ((word = words.pop())) {
          line.push(word);
          tspan.text(line.join(' '));
          if (tspan.node().getComputedTextLength() > width) {
            line.pop();
            tspan.text(line.join(' '));
            line = [word];
            tspan = text
              .append('tspan')
              .attr('x', x)
              .attr('y', y)
              .attr('dy', ++lineNumber * lineHeight + dy + 'em')
              .text(word);
          }
        }
      });
    } //wrap
  }
  ngOnInit(): void {
    this.margin = { top: 10, right: 5, bottom: 12, left: 15 };
    this.width = 620;
    this.height = 630;

    //data modification
    this.songData = this.mood_data;

    for (let i = 0; i < this.songData.length; i++) {
      this.trackColors.push(this.songData[i].trackColor);
      //each json or song
      let allScores = [];

      let calm = 0;
      let dark = 0;
      let energetic = 0;
      let happy = 0;
      let melancholic = 0;
      let relaxed = 0;
      let tense = 0;
      let uplifting = 0;
      for (let j = 0; j < this.songData[i].segments.length; j++) {
        allScores.push(this.songData[i].segments[j].analysis.scores);
      }

      for (let j = 0; j < allScores.length; j++) {
        calm = calm + allScores[j].calm;
        dark = dark + allScores[j].dark;
        energetic = energetic + allScores[j].energetic;
        happy = happy + allScores[j].happy;
        melancholic = melancholic + allScores[j].melancholic;
        relaxed = relaxed + allScores[j].relaxed;
        tense = tense + allScores[j].tense;
        uplifting = uplifting + allScores[j].uplifting;
      }
      this.finalData.push([
        { axis: 'Calm', value: (calm / 8).toFixed(2) },
        { axis: 'Dark', value: (dark / 8).toFixed(2) },
        { axis: 'Energetic', value: (energetic / 8).toFixed(2) },
        { axis: 'Happy', value: (happy / 8).toFixed(2) },
        { axis: 'Melancholic', value: (melancholic / 8).toFixed(2) },
        { axis: 'Relaxed', value: (relaxed / 8).toFixed(2) },
        { axis: 'Tense', value: (tense / 8).toFixed(2) },
        { axis: 'Uplifting', value: (uplifting / 8).toFixed(2) },
      ]);
    }
    console.log(this.finalData);
    let color = d3
      .scaleOrdinal()
      .range(['red', 'yellow', 'pink', 'blue', 'black']);
    // let color = d3.scaleOrdinal().range(['red', 'yellow']);
    // let color = d3.scaleOrdinal().range(this.trackColors);

    this.radarChartOptions = {
      w: this.width,
      h: this.height,
      margin: this.margin,
      maxValue: 0.5,
      levels: 1,
      roundStrokes: true,
      color: color,
    };
    this.RadarChart('.radarChart', this.finalData, this.radarChartOptions);
  }
}
